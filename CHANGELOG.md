# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.3.0] - 2018-12-29

### Added

- `setTimeOut()` function.

## [0.2.0] - 2018-09-24

### Added
- `getPercent()` function.
- `getProtocol()` function.
- `isHTTP()` function.
- `isFile()` function.

## [0.1.0-SNAPSHOT] - 2018-04-26

### Added
- `getETA()` function.
- `getSpeed()` function.
- `continueDownload()` function.

### Changed
- Listeners to pass `EzDownloader` as parameter.
