# *EzDownloader*

## EzDownloader is

An EzCore's module that lets you simply download files.

## EzDownloader has

**Listeners**: The download can be listened when starts, ends, cancel, error, finalize and in progress.<br>
**Synchronous**: The download can be synchronous or asynchronous.<br>
**Functions**: There are many functions, for example `getDownloadSpeed()`, `getETA()`, `getFileSize()` and `getReamingBytes()`.<br>
**Converted**: The size can be converted to Kilobytes, Megabytes and Gigabytes.<br>
**Format**: Makes the values more readable .<br>
**Custom User Agent**: The user agent is customizable.<br>
**Protocols**: Many protocols supported: HTTP, HTTPS, FILE, FTP.<br>
**Custom Chunk Size**: The chunk size is customizable.<br>
**Resume the Download**: The download can be resumed.<br>
**Simple**: It's simple to use.<br>
**Default Options**: If you don't care about the listed things, then you can just use default options, they work just fine.

## Using EzDownloader
See the wiki: On Building

## Build EzCore
See the wiki: On Building

## Using with Maven:
See the wiki: On Building

## Changelog
[See the CHANGELOG file](./CHANGELOG.md).

## Discord Server
[![Discord](https://discordapp.com/api/guilds/317108460359516160/embed.png?style=banner4)](https://discord.gg/5wjHmcQ)

## License
[![LGPL](https://www.gnu.org/graphics/lgplv3-with-text-154x68.png)](https://www.gnu.org/copyleft/lesser.html)  
See the [COPYING](./COPYING) and [COPYING.LESSER](./COPYING.LESSER) files.
