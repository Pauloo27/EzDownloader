package com.gitlab.pauloo27.core.downloader;

/**
 * <h1>EzDownloaderListener</h1>
 * <p>EzDownloader's Listener. You can listen to when the download starts, progresses, finalize, errors or gets canceled.</p>
 *
 * @author Paulo
 * @version 1.0
 * @since 0.0.2
 */
public interface EzDownloaderListener {

    /**
     * Called when the download starts.
     *
     * @param downloader The EzDownloader instance.
     */
    void onStart(EzDownloader downloader);

    /**
     * Called in every download progress.
     *
     * @param downloader The EzDownloader instance.
     */
    void onProgress(EzDownloader downloader);

    /**
     * Called when the download is finalized.
     *
     * @param downloader The EzDownloader instance.
     */
    void onFinalize(EzDownloader downloader);

    /**
     * Called when the download is canceled.
     *
     * @param downloader The EzDownloader instance.
     */
    void onCancel(EzDownloader downloader);

    /**
     * Called when the download has some error.
     *
     * @param downloader The EzDownloader instance.
     * @param e          The exception.
     */
    void onError(EzDownloader downloader, Exception e);
}
