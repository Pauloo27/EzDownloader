package com.gitlab.pauloo27.core.downloader;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * EzDownloader is a simple way to download files.
 *
 * @author Paulo
 * @version 2.0
 * @since 0.0.1
 */
public class EzDownloader {

    /**
     * Decimal format with pattern {@code ####0.00}.
     */
    private static DecimalFormat df = new DecimalFormat("####0.00");
    /**
     * The download's URL.
     */
    private URL url;
    /**
     * The download's output file.
     */
    private File file;
    /**
     * The download's output stream.
     */
    private OutputStream out;
    /**
     * The download's input stream.
     */
    private InputStream in;
    /**
     * The download's listeners.
     */
    private ArrayList<EzDownloaderListener> listeners = new ArrayList<>();
    /**
     * If the download is running.
     */
    private boolean downloading = false;
    /**
     * The download's type.
     */
    private EzDownloadType type;
    /**
     * The download's thread.
     */
    private Thread downloadThread;
    /**
     * The download's start in milliseconds.
     */
    private long starts = -1;
    /**
     * The download's end in milliseconds.
     */
    private long ends = -1;
    /**
     * The download's file size.
     */
    private double fileSize = -1;
    /**
     * The download's last downloaded byte.
     */
    private long lastDownloadedBytes = 0;
    /**
     * The download's speed.
     */
    private long speed = 0;
    /**
     * The download's bytes per chunk.
     */
    private int bytesPerChunk = 2048;
    /**
     * The download's user agent.
     */
    private String userAgent = "Mozilla/4.76";
    /**
     * If the download is a continuation.
     */
    private boolean continuation = false;
    /**
     * The connection time out.
     */
    private int timeOut = 10 * 1000;

    /**
     * Builds a downloader.
     *
     * @param url         The URL to the file that will be downloaded.
     * @param destination The file where the download will be saved.
     */
    public EzDownloader(URL url, File destination) {
        this.file = destination;
        this.url = url;
    }

    /**
     * Continues another download.
     *
     * @param url         The URL to the file that will be downloaded.
     * @param destination The file where the download will be saved.
     * @return The new EzDownloader instance.
     */
    public static EzDownloader continueDownload(URL url, File destination) {
        EzDownloader downloader = new EzDownloader(url, destination);
        downloader.continuation = true;
        return downloader;
    }

    /**
     * Converts bytes to kilobytes.
     *
     * @param bytes The size in bytes.
     * @return The size in kilobytes.
     */
    public static float toKilobytes(float bytes) {
        return (bytes / 1024);
    }

    /**
     * Converts bytes to megabytes.
     *
     * @param bytes The size in bytes.
     * @return The size in megabytes.
     */
    public static float toMegabytes(float bytes) {
        return (bytes / 1024) / 1024;
    }

    /**
     * Converts bytes to gigabytes.
     *
     * @param bytes The size in bytes.
     * @return The size in gigabytes.
     */
    public static float toGigabytes(float bytes) {
        return ((bytes / 1024) / 1024) / 1024;
    }

    /**
     * Formats a floating number using pattern "####0.00".
     *
     * @param value Value to be formatted
     * @return The value formatted in String.
     */
    public static String format(float value) {
        return df.format(value);
    }

    /**
     * Starts the download.
     *
     * @param type The type of download.
     */
    public void start(EzDownloadType type) {
        this.type = type;
        if (type.isAsync()) {
            downloadThread = new Thread(new DownloadRunnable(), "Download Thread");
            downloadThread.start();
        } else {
            new DownloadRunnable().run();
        }
    }

    /**
     * Adds one or more listener(s) to the download.
     *
     * @param downloaderListeners Array of EzDownloaderListener.
     */
    public void addListener(EzDownloaderListener... downloaderListeners) {
        listeners.addAll(Arrays.asList(downloaderListeners));
    }

    /**
     * Removes one or more listener(s) from the download.
     *
     * @param downloaderListeners Array of EzDownloaderListener.
     */
    public void removeListener(EzDownloaderListener... downloaderListeners) {
        listeners.removeAll(Arrays.asList(downloaderListeners));
    }

    /**
     * Gets the total of downloaded bytes.
     *
     * @return The number of the downloaded bytes.
     */
    public long getDownloadedBytes() {
        return file.length();
    }

    /**
     * Gets when the download starts in milliseconds. If the download hasn't started yet, return -1.
     *
     * @return The system milliseconds when the download has started.
     */
    public long getStarts() {
        return starts;
    }

    /**
     * Gets when the download ends in milliseconds. If the download hasn't ended yet, return -1.
     *
     * @return The system milliseconds when the download has ended.
     */
    public long getEnds() {
        return ends;
    }

    /**
     * Gets the time of download.
     *
     * @return The difference between the system milliseconds when the download
     * ended and when the download started if the download has ended, else return the difference
     * between current system milliseconds and the system milliseconds when the download started.
     */
    public long getDownloadTime() {
        if (ends == -1) {
            return System.currentTimeMillis() - starts;
        } else {
            return ends - starts;
        }
    }

    /**
     * Sets the bytes to download per chunk. This amount of bytes will be downloaded to memory, saved to the output
     * file, and repeat until the download is finished. The default is 2048 bytes.
     *
     * @param bytesPerChunk The bytes to be download per chunk.
     */
    public void setBytesPerChunk(int bytesPerChunk) {
        this.bytesPerChunk = bytesPerChunk;
    }

    /**
     * Sets the timeout in seconds. In case of time out the
     * {@link EzDownloaderListener#onError(EzDownloader, Exception)} will be called. The default value is 10 seconds.
     *
     * @param timeOut The time in seconds.
     */
    public void setTimeOut(int timeOut) {
        this.timeOut = timeOut;
    }

    /**
     * Sets the user agent used to connect. Default user agent is "Mozilla/4.76".
     *
     * @param userAgent The user agent. If null, user agent will not be used.
     */
    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    /**
     * Gets the Estimated Time of Arrival.
     *
     * @return The Estimated Time of Arrival.
     */
    public long getETA() {
        if (this.getReamingBytes() == 0 || this.getDownloadSpeed() == 0) {
            return 0;
        }
        return getReamingBytes() / getDownloadSpeed();
    }

    /**
     * Gets the download protocol.
     *
     * @return The URL protocol.
     */
    public String getProtocol() {
        return url.getProtocol();
    }

    /**
     * Checks if the protocol is HTTP or HTTPS.
     *
     * @return If the protocol is HTTP or HTTPS.
     */
    public boolean isHTTP() {
        return getProtocol().equalsIgnoreCase("https") || getProtocol().equalsIgnoreCase("http");
    }

    /**
     * Checks if the protocol is file.
     *
     * @return If the protocol is file.
     */
    public boolean isFile() {
        return getProtocol().equalsIgnoreCase("file");
    }

    /**
     * Gets all listeners from the download.
     *
     * @return The list of EzDownloaderListener.
     */
    public List<EzDownloaderListener> getListeners() {
        return listeners;
    }

    /**
     * Gets the size of the download's file. This returns the size in bytes, you can convert using
     * {@link #toKilobytes(float)}, {@link #toMegabytes(float)} and {@link #toGigabytes(float)}.
     *
     * @return The size of file in bytes.
     */
    public long getFileSize() {
        if (fileSize == -1) {
            try {
                URLConnection connection = url.openConnection();
                // Verify if the protocol is http or https
                if (connection instanceof HttpURLConnection) {
                    HttpURLConnection httpCon = (HttpURLConnection) connection;
                    // Add User-Agent to avoid 403 error.
                    if (userAgent != null)
                        httpCon.addRequestProperty("User-Agent", userAgent);
                    fileSize = httpCon.getContentLength();
                } else {
                    fileSize = connection.getContentLength();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return (long) fileSize;
    }

    /**
     * Gets the download speed.
     *
     * @return The speed of the download in bytes per second.
     */
    public long getDownloadSpeed() {
        return speed;
    }

    /**
     * Gets the download progress percent.
     *
     * @return The download progress percent.
     */
    public int getDownloadPercent() {
        if (this.getDownloadedBytes() == 0 || this.getFileSize() == 0)
            return 0;
        return (int) ((this.getDownloadedBytes() * 100) / getFileSize());
    }

    /**
     * Gets the reaming bytes.
     *
     * @return The reaming bytes.
     */
    public long getReamingBytes() {
        return this.getFileSize() - this.getDownloadedBytes();
    }

    /**
     * Gets the download input stream.
     *
     * @return The Stream where the bytes are read.
     */
    public InputStream getInputStream() {
        return in;
    }

    /**
     * Gets the download output stream.
     *
     * @return The Stream where the bytes are written.
     */
    public OutputStream getOutputStream() {
        return out;
    }

    /**
     * Gets the download URL.
     *
     * @return The URL of the file.
     */
    public URL getURL() {
        return url;
    }

    /**
     * Checks if the download is alive.
     *
     * @return If the thread of the Async download is Alive. If the download isn't async, return false.
     */
    public boolean isAlive() {
        if (!isAsync()) return false;
        return downloadThread.isAlive();
    }

    /**
     * Gets the download's output file.
     *
     * @return The File where the download is saved.
     */
    public File getFile() {
        return file;
    }

    /**
     * Checks if the download is running.
     *
     * @return If the download is running.
     */
    public boolean isDownloading() {
        return downloading;
    }

    /**
     * Checks if the download is async.
     *
     * @return If the download is async.
     */
    public boolean isAsync() {
        return this.type.isAsync();
    }

    /**
     * Checks if the download uses soft progress.
     *
     * @return If the download uses SoftProgress.
     * @see EzDownloadType
     */
    public boolean isSoftProgress() {
        return this.type.isSoftProgress();
    }

    /**
     * Gets the download type.
     *
     * @return The type of download.
     */
    public EzDownloadType getType() {
        return this.type;
    }

    /**
     * Cancels the Download.
     */
    public void cancelDownload() {
        downloading = false;
    }

    /**
     * Gets the download file path.
     *
     * @return The protocol + host + path.
     */
    public String getFilePath() {
        return url.getProtocol() + "://" + url.getHost() + url.getPath();
    }

    /**
     * Checks if the download is a continuation of another download.
     *
     * @return If this download is a continuation of another download.
     */
    public boolean isContinuation() {
        return continuation;
    }

    /**
     * Called when the download starts. Call all listeners.
     */
    private void onStart() {
        downloading = true;
        starts = System.currentTimeMillis();
        listeners.forEach(downloaderListener -> downloaderListener.onStart(this));
    }

    /**
     * Called when the download have a progress. Call all listeners.
     */
    private void onProgress() {
        listeners.forEach(listener -> listener.onProgress(this));
    }

    /**
     * Called when the download ends without errors. Call all listeners.
     */
    private void onFinalize() {
        downloading = false;
        ends = System.currentTimeMillis();
        listeners.forEach(listener -> listener.onFinalize(this));
    }

    /**
     * Called when the download is canceled. Call all listeners.
     */
    private void onCancel() {
        downloading = false;
        ends = System.currentTimeMillis();
        listeners.forEach(listener -> listener.onCancel(this));
    }

    /**
     * Called when the download have an error. Call all listeners.
     *
     * @param e The error.
     */
    private void onError(Exception e) {
        downloading = false;
        ends = System.currentTimeMillis();
        listeners.forEach(listener -> listener.onError(this, e));
    }

    /**
     * The type of download.
     * Choose if the download is sync or async and if the download use SoftProgress.
     * <h1>What's SoftDownload?</h1>
     * With SoftDownload the listeners of {@link EzDownloaderListener#onProgress(EzDownloader)}
     * will be called every second. With this, they will be called in every download chunk.
     */
    public enum EzDownloadType {
        /**
         * Synchronous and no SoftProgress.
         */
        SYNC,
        /**
         * Asynchronous and no SoftProgress.
         */
        ASYNC,
        /**
         * Synchronous and SoftProgress.
         */
        SYNC_SOFT_PROGRESS,
        /**
         * Asynchronous and SoftProgress.
         */
        ASYNC_SOFT_PROGRESS;

        /**
         * Checks if the type download type is async.
         *
         * @return If is Asynchronous ({@link #ASYNC} or {@link #ASYNC_SOFT_PROGRESS}).
         */
        public boolean isAsync() {
            return (this == ASYNC || this == ASYNC_SOFT_PROGRESS);
        }

        /**
         * Checks if the download type is soft progress.
         *
         * @return If use SoftDownload ({@link #ASYNC_SOFT_PROGRESS} or {@link #SYNC_SOFT_PROGRESS}).
         */
        public boolean isSoftProgress() {
            return (this == ASYNC_SOFT_PROGRESS || this == SYNC_SOFT_PROGRESS);
        }

    }

    /**
     * The download runnable.
     */
    private class DownloadRunnable implements Runnable {
        @Override
        public void run() {
            try {
                URLConnection connection = url.openConnection();
                connection.setConnectTimeout(timeOut);
                connection.setReadTimeout(timeOut);
                // Verify if the protocol is http or https
                if (connection instanceof HttpURLConnection) {
                    HttpURLConnection httpCon = (HttpURLConnection) connection;
                    // Add User-Agent to avoid 403 error.
                    if (userAgent != null)
                        httpCon.addRequestProperty("User-Agent", userAgent);
                    in = httpCon.getInputStream();
                } else {
                    in = connection.getInputStream();
                }

                if (isContinuation())
                    in.skip(file.length());

                out = new FileOutputStream(file, isContinuation());
                EzDownloader.this.lastDownloadedBytes = file.length();

                onStart();
                Thread speedThread = new Thread(new SpeedRunnable(), "Speed Thread");
                speedThread.start();
                if (isSoftProgress()) {
                    Thread progressThread = new Thread(new ProgressRunnable(), "Progress Thread");
                    progressThread.start();
                }

                byte[] b = new byte[bytesPerChunk];
                int length;
                while ((length = in.read(b)) != -1 && downloading) {
                    if (!isSoftProgress()) {
                        onProgress();
                    }
                    out.write(b, 0, length);
                }
                if (in.read(b) == -1) {
                    onFinalize();
                } else {
                    onCancel();
                }
                in.close();
                out.close();
            } catch (IOException e) {
                onError(e);
            }
        }
    }

    /**
     * The download speed calculator runnable.
     */
    private class SpeedRunnable implements Runnable {
        @Override
        public void run() {
            while (isDownloading()) {
                if (getDownloadedBytes() == 0) {
                    speed = 0;
                } else {
                    speed = (getDownloadedBytes() - lastDownloadedBytes);
                }
                lastDownloadedBytes = getDownloadedBytes();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * The download soft progress runnable.
     */
    private class ProgressRunnable implements Runnable {
        @Override
        public void run() {
            while (isDownloading()) {
                onProgress();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
