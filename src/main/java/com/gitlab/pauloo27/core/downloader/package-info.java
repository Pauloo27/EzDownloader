/**
 * Package with all EzDownloader classes.
 *
 * @author Paulo
 * @version 2.0
 * @since 0.0.1
 */
package com.gitlab.pauloo27.core.downloader;