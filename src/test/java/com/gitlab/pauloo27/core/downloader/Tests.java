package com.gitlab.pauloo27.core.downloader;

import org.junit.Assert;
import org.junit.Test;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Tests {

    private static final String httpFileUrl = "https://pauloo27.gitlab.io/EzDownloader/1M.zip";
    private static final String httpHashUrl = "https://pauloo27.gitlab.io/EzDownloader/checksum.txt";
    private static String fileHash;
    private static final File saveFile = new File("Downloaded.zip");
    //    private static final String ftpFile = "ftp://speedtest.tele2.net/1MB.zip";
    private static final String localFile = "file:///etc/fstab";
    private static EzDownloader downloader;
    private static boolean stop = false;

    @Test
    public void testSyncDownload() throws MalformedURLException {
        System.out.println("\n====Testing SyncDownloader====\n");
        downloader = new EzDownloader(new URL(httpFileUrl), saveFile);
        downloader.addListener(new Listener());
        downloader.start(EzDownloader.EzDownloadType.SYNC);
    }

    @Test
    public void testSyncSoftProgressDownload() throws MalformedURLException {
        System.out.println("\n====Testing SyncDownloader SoftProgress====\n");
        downloader = new EzDownloader(new URL(httpFileUrl), saveFile);
        downloader.addListener(new Listener());
        downloader.start(EzDownloader.EzDownloadType.SYNC_SOFT_PROGRESS);
    }

    @Test
    public void testASyncDownload() throws MalformedURLException {
        System.out.println("\n====Testing AsyncDownloader====\n");
        downloader = new EzDownloader(new URL(httpFileUrl), saveFile);
        downloader.addListener(new Listener());
        downloader.start(EzDownloader.EzDownloadType.ASYNC);
        while (downloader.isAlive()) {
        }
    }

    @Test
    public void testASyncSoftProgressDownload() throws MalformedURLException {
        System.out.println("\n====Testing AsyncDownloader SoftProgress====\n");
        downloader = new EzDownloader(new URL(httpFileUrl), saveFile);
        downloader.addListener(new Listener());
        downloader.start(EzDownloader.EzDownloadType.ASYNC_SOFT_PROGRESS);
        while (downloader.isAlive()) {
        }
    }

    @Test
    public void testContinueDownload() throws MalformedURLException {
        System.out.println("\n====Testing ContinueDownload====\n");
        stop = true;
        downloader = new EzDownloader(new URL(httpFileUrl), saveFile);
        downloader.addListener(new Listener());
        downloader.start(EzDownloader.EzDownloadType.SYNC_SOFT_PROGRESS);
        downloader = EzDownloader.continueDownload(new URL(httpFileUrl), saveFile);
        downloader.addListener(new Listener());
        downloader.start(EzDownloader.EzDownloadType.SYNC_SOFT_PROGRESS);
    }

    @Test
    public void testLocalFile() throws MalformedURLException {
        System.out.println("\n====Testing LocalFile====\n");
        downloader = new EzDownloader(new URL(localFile), new File("LocalFile"));
        downloader.addListener(new Listener());
        downloader.start(EzDownloader.EzDownloadType.SYNC_SOFT_PROGRESS);
    }

    /* TODO Fix FTP Test
    @Test
    public void testFTP() throws MalformedURLException {
        System.out.println("\n====Testing FTP====\n");
        downloader = new EzDownloader(new URL(ftpFile), saveFile);
        downloader.addListener(new Listener());
        downloader.start(EzDownloader.EzDownloadType.SYNC_SOFT_PROGRESS);
    }
     */

    private class Listener implements EzDownloaderListener {

        @Override
        public void onStart(EzDownloader downloader) {
            System.out.printf("%s | Size: %d B%n", (downloader.isContinuation() ? "Restoring" : "Downloading"), downloader.getFileSize());
        }

        @Override
        public void onProgress(EzDownloader downloader) {
            System.out.printf("%d/%d B | Time: %d s | ETA: %d s | %s kB/s | %d%%%n",
                    downloader.getDownloadedBytes(),
                    downloader.getFileSize(),
                    ((System.currentTimeMillis() - downloader.getStarts()) / 1000),
                    downloader.getETA(),
                    EzDownloader.format(EzDownloader.toKilobytes(downloader.getDownloadSpeed())),
                    downloader.getDownloadPercent()
            );
            if (stop && downloader.getDownloadedBytes() >= (downloader.getFileSize() / 2)) {
                downloader.cancelDownload();
            }
        }

        @Override
        public void onFinalize(EzDownloader downloader) {
            System.out.printf("Finalized! Time: %d s%n", downloader.getDownloadTime() / 1000);
            System.out.printf("%s B | %s B%n", downloader.getStarts() / 1000, downloader.getEnds() / 1000);
            if (!downloader.isFile()) {
                System.out.println("Checking hash...");
                String hash = null;
                String expectedHash = null;
                try {
                    expectedHash = getFileHash();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    hash = hashFileToString(downloader.getFile());
                    System.out.printf("Hash = %s | Expected = %s%n", hash, fileHash);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Assert.assertEquals(expectedHash, hash);
            }
            System.out.println("Deleting file...");
            downloader.getFile().delete();
        }

        @Override
        public void onCancel(EzDownloader downloader) {
            System.out.println("Download cancelled.");
            if (stop) {
                stop = false;
                System.out.println("Paused");
            } else {
                System.out.println("Deleting file...");
                downloader.getFile().delete();
            }
        }

        @Override
        public void onError(EzDownloader downloader, Exception e) {
            System.out.println("An error occurred:");
            e.printStackTrace();
            System.out.println("Deleting file...");
            downloader.getFile().delete();
        }
    }

    private static String getFileHash() throws IOException {
        if (fileHash != null)
            return fileHash;

        return fileHash = getRawContent(new URL(httpHashUrl)).split(" ")[0];
    }

    private static String hashFileToString(File file) throws IOException, NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(Files.readAllBytes(file.toPath()));
        byte[] hash = md.digest();
        StringBuilder sb = new StringBuilder();
        for (byte b : hash) {
            sb.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

    private static String getRawContent(URL url) throws IOException {
        URLConnection connection = url.openConnection();
        connection.setReadTimeout(10 * 1000);
        connection.setConnectTimeout(10 * 1000);
        InputStream in;
        // Verify if the protocol is http or https
        if (connection instanceof HttpURLConnection) {
            HttpURLConnection httpCon = (HttpURLConnection) connection;
            // Add User-Agent to avoid 403 error.
            httpCon.addRequestProperty("User-Agent", "Mozilla/4.76");
            in = httpCon.getInputStream();
        } else {
            in = connection.getInputStream();
        }
        BufferedReader br = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
        StringBuilder str = new StringBuilder();
        br.lines().forEach(line -> str.append(str.length() == 0 ? line : "\n" + line));
        in.close();
        br.close();
        return str.toString();
    }

}
