package com.gitlab.pauloo27.core.downloader.samples;

import com.gitlab.pauloo27.core.downloader.EzDownloader;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class SimpleDownload {

    public static void main(String[] args) throws MalformedURLException {
        URL url = new URL("https://gist.githubusercontent.com/Pauloo27/8c40b4cefdb6d018c20079d65e1c2723/raw/1e0e66ca80a21bd30106f0b6c68283a49471fc06/HelloWorld.py"); // Instances the File URL
        File file = new File("Downloaded.txt"); // Instances the file
        EzDownloader downloader = new EzDownloader(url, file); // Prepares the download
        downloader.start(EzDownloader.EzDownloadType.SYNC_SOFT_PROGRESS); // Starts the download
    }

}
