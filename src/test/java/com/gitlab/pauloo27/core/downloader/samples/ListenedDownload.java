package com.gitlab.pauloo27.core.downloader.samples;

import com.gitlab.pauloo27.core.downloader.EzDownloader;
import com.gitlab.pauloo27.core.downloader.EzDownloaderListener;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class ListenedDownload {

    public static void main(String[] args) throws MalformedURLException {
        URL url = new URL("https://gist.githubusercontent.com/Pauloo27/8c40b4cefdb6d018c20079d65e1c2723/raw/1e0e66ca80a21bd30106f0b6c68283a49471fc06/HelloWorld.py"); // Instances the File URL
        File file = new File("Downloaded.txt"); // Instances the file
        EzDownloader downloader = new EzDownloader(url, file); // Prepares the download
        downloader.addListener( // Adds a listener
                new EzDownloaderListener() { // Creates the listener
                    @Override
                    public void onStart(EzDownloader downloader) {
                        System.out.printf("Downloading file %s%n", downloader.getFilePath());
                    }

                    @Override
                    public void onProgress(EzDownloader downloader) {
                        System.out.printf("%d/%d B | Time: %d s | ETA: %d s | %s kB/s | %d%%%n",
                                downloader.getDownloadedBytes(),
                                downloader.getFileSize(),
                                ((System.currentTimeMillis() - downloader.getStarts()) / 1000),
                                downloader.getETA(),
                                EzDownloader.format(EzDownloader.toKilobytes(downloader.getDownloadSpeed())),
                                downloader.getDownloadPercent()
                        );
                    }

                    @Override
                    public void onFinalize(EzDownloader downloader) {
                        System.out.printf("Finalized! Time: %d milliseconds%n", +downloader.getDownloadTime());
                    }

                    @Override
                    public void onCancel(EzDownloader downloader) {
                        System.out.println("Canceled!");
                    }

                    @Override
                    public void onError(EzDownloader downloader, Exception e) {
                        System.out.println("Error: ");
                        e.printStackTrace();
                    }
                });
        downloader.start(EzDownloader.EzDownloadType.SYNC_SOFT_PROGRESS); // Starts the download
    }

}
